import {
  BrowserRouter,
  Routes,
  Route,
  Outlet,
  useNavigate,
} from "react-router-dom";

import { Provider, useDispatch, useSelector } from "react-redux";
import { persistor, store } from "./store";
import SignIn from "./users/SignIn";
import { logOut } from './store/user'
import { PersistGate } from "redux-persist/integration/react";
import Videos from "./videos/Videos.jsx";
import VideosForm from "./videos/VideosFrom";
import VideoShow from "./videos/VideoShow";
import Profile from "./users/Profile";

let NotImplemented = () => {
  return <h1>Esta página aún no está lista!</h1>;
};

let UsuariosOutlet = () => {
  let user = useSelector( state => state.user.user )
  let dispatch = useDispatch()
  let navigate = useNavigate();
  let doLogOut = () => {
    dispatch(
      logOut()
    )
    navigate('/usuarios/login')
  }
  return (
    <>{ user && <button onClick={doLogOut}>Cerrar sesión</button>}
      <Outlet />
    </>
  );
};
function App() {
  return (
    <BrowserRouter>
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          <Routes>
            <Route path="/" element={<NotImplemented />} />

            <Route path="/usuarios" element={<UsuariosOutlet />}>
              <Route path="registro" element={<NotImplemented />} />
              <Route path="login" element={<SignIn />} />
              <Route path="miperfil" element={<Profile />} />
              <Route path=":id/videos" element={<NotImplemented />} />
            </Route>

            <Route path="/videos">
              <Route path="/" element={<Videos />} />
              <Route path=":id" element={<VideoShow />} />
              <Route path="nuevo" element={<VideosForm />} />
            </Route>
            
          </Routes>
        </PersistGate>
      </Provider>
    </BrowserRouter>
  );
}

export default App;
